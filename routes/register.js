// Server port
const serverPort = process.env.SERVER_PORT || 80

// Setup express
const express = require("express")
const router = express.Router()

// Setup node-fetch
const nodeFetch = require("node-fetch")

// Setup random key generation
const randomKey = require("random-key-generator")


// Register page
router.get("/", (req, res) => {
    res.render("register", {title: "Register"})
})


// Register POST
router.post("/", (req, res) => {

    // Ensure hostname is filled
    if(req.body.host === null || typeof req.body.host === "undefined" || req.body.host == "") {
        res.redirect("/register")
    } else {

        // Verify passwords are the same
        if (req.body.password === req.body.confirm_password) {

            let hostname = "http://" + req.body.host + ":" + serverPort
            let host = hostname + "/auth/register"
            let key = randomKey()

            // POST request to server
            fetch(host, {
                method: "POST",
                body: JSON.stringify({"user": req.body.username, "pass": req.body.password, "key": key}),
                headers: { 'Content-Type': 'application/json' }

            // Response
            }).then(() => {
                res.cookie("key", key)
                res.cookie("username", req.body.username)
                res.redirect("/dashboard")
            })

        } else {
            res.redirect("/register")
        }
    }
})


// Export
module.exports = router