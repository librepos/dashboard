// Setup express
const express = require("express")
const router = express.Router()

// Setup node-fetch
const nodeFetch = require("node-fetch")


// Verification
router.use((req, res, next) => {
    if (req.cookies.key === undefined || req.cookies.key === null) {
        res.redirect("/login")
    } else {
        next()
    }
})

// Home
router.get("/", (req, res) => {
    res.render("dashboard/home", {title: "Dashboard", username: req.cookies.username})
})


// Finance
router.get("/finance(/:list)?", (req, res) => {
    res.render("dashboard/finance", {title: "Finance", username: req.cookies.username})
})


// Inventory
router.get("/inventory(/:list)?", (req, res) => {
    if(req.params.list === undefined) {
        res.redirect("/dashboard/inventory/all")
    } else {

        // POST request to server
        fetch(req.cookies.host + "/data/products/get", {
            method: "POST",
            body: JSON.stringify({"user": req.cookies.username, "key": req.cookies.key}),
            headers: { 'Content-Type': 'application/json' }

        // Response
        }).then(res => res.json()).then(json => {
            res.render("dashboard/inventory", {title: "Inventory", username: req.cookies.username, page: req.params.list, data: json})
        })
    }
})


// Staff
router.get("/staff(/:list)?", (req, res) => {
    res.render("dashboard/staff", {title: "Staff", username: req.cookies.username})
})


// Export
module.exports = router