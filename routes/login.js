// Server port
const serverPort = process.env.SERVER_PORT || 3353

// Setup express
const express = require("express")
const router = express.Router()

// Setup node-fetch
const nodeFetch = require("node-fetch")

// Setup random-key-generator
const randomKey = require("random-key-generator")


// Login page
router.get("/", (req, res) => {
    res.render("login", {title: "Login"})
})


// Login POST
router.post("/", (req, res) => {
    console.log(req.body.host)

    // Ensure hostname is filled
    if(req.body.host === null || typeof req.body.host === "undefined" || req.body.host == "") {
        res.redirect("/login")
    } else {

        // Delete any cookies
        res.clearCookie("key")
        
        let hostname = "http://" + req.body.host + ":" + serverPort
        let host = hostname + "/auth/login"
        let key = randomKey()

        // POST request to server
        fetch(host, {
            method: "POST",
            body: JSON.stringify({"user": req.body.username, "pass": req.body.password, "key": key}),
            headers: { 'Content-Type': 'application/json' }

        // Response
        }).then(res => res.json()).then(json => {

            // If correct
            if(json.logged == true) {

                // Set cookie and redirect
                res.cookie("key", key)
                res.cookie("username", req.body.username)
                res.cookie("host", hostname)
                res.redirect("/dashboard")

            // Incorrect
            } else {

                // Go back to login page
                res.redirect("/login")
            }
        })
    }
})

// Logout
router.get("/out", (req, res) => {
    res.clearCookie("key")
    res.clearCookie("username")
    res.redirect("/")
})


// Export
module.exports = router