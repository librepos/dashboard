# LibrePOS Admin Dashboard
This is the repository for the LibrePOS admin dashboard. It allows you to connect to and monitor your LibrePOS server.

By default, it runs on port 80, however this can be overriden with the PORT env variable.
