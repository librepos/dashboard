// Setup env variables
require('dotenv').config()

// Setup express
const express = require("express")
const app = express()
const port = process.env.PORT || 80

// Setup body-parser & cookie-parser
const bodyParser = require("body-parser")
const cookieParser = require("cookie-parser")
app.use(bodyParser.urlencoded({extended: true}))
app.use(cookieParser())

// Setup node-fetch
const nodeFetch = require("node-fetch")

// Setup pug
app.use(express.static("./public"))
app.set("views", "./views")
app.set("view engine", "pug")


// Login routing
app.get("/", (req, res) => {

    // If there is a key
    if(req.cookies.key) {
        res.redirect("/dashboard")
    } else {
        res.redirect("/login")
    }
})


// Login page
const login = require("./routes/login")
app.use("/login", login)

// Register page
const register = require("./routes/register")
app.use("/register", register)


// Dashboard page
const dash = require("./routes/dashboard")
app.use("/dashboard", dash)


// Run server
app.listen(port, () => {
    console.log("[Server] Running on port " + port)
})