FROM node:18-alpine

RUN addgroup app && adduser -S -G app app

RUN mkdir /app && chown -R node:node /app

WORKDIR /app

COPY --chown=node:node package*.json ./

RUN npm install

COPY --chown=node:node . .

USER node

EXPOSE 80

CMD [ "node", "server.js"]
